package controller;

import exceptions.FileNotFoundException;
import exceptions.InvalidInputException;
import java.util.List;
import model.Contact;
import service.ContactService;
import service.impl.ContactServiceImpl;

public class ContactController {

  private final ContactService contactService = new ContactServiceImpl();

  public void create(final Contact contact) throws FileNotFoundException, InvalidInputException {
    contactService.create(contact);
  }

  public void delete(final Integer id) throws FileNotFoundException {
    contactService.delete(id);
  }

  public void update(final Contact contact) throws FileNotFoundException {
    contactService.update(contact);
  }

  public final List<Contact> getAllContacts() throws FileNotFoundException {
    return contactService.getAllContacts();
  }
}
