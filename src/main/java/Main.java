import static view.MenuView.viewMenu;

import exceptions.ExceededCharacterLimitException;
import exceptions.FileNotFoundException;
import exceptions.InvalidInputException;

public class Main {

  public static void main(String[] args) {
    try {
      viewMenu();
    } catch (FileNotFoundException | ExceededCharacterLimitException | InvalidInputException e) {
      e.printStackTrace();
    }
  }
}
