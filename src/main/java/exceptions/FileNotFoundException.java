package exceptions;

import java.io.IOException;

public class FileNotFoundException extends IOException {

  public FileNotFoundException(final String fileNamePath) {
    super(String.format("File with path %s was not found", fileNamePath));
  }

}
