package exceptions;

public class InvalidInputException extends RuntimeException {

    public InvalidInputException(final String message) {
      super(String.format("Invalid %s", message));
    }
  }
