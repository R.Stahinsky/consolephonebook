package exceptions;

public class ExceededCharacterLimitException extends Exception {

  public ExceededCharacterLimitException() {
    super("Input length exceeded the limit of 255 characters");
  }
}
