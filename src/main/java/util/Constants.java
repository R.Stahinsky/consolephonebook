package util;

import java.util.Scanner;

public class Constants {

  public static final String STARS = "********";
  public static final String PRESS = "Press";

  public static final String CONTACTS = String.format("%s Contacts %s ", STARS, STARS);
  public static final String CREATE = String.format("%s %n Create ", STARS);
  public static final String CREATE_CONTACT = String.format("%s New contact %s", STARS, STARS);
  public static final String ENTER_CONTACT_ID = "Enter contact id: ";
  public static final String GOODBYE = String.format("%s Goodbye %s", STARS, STARS);
  public static final Scanner SCANNER = new Scanner(System.in);
  public static final Scanner SCANNER_INTEGER = new Scanner(System.in);
  public static final String WELCOME = String.format("%n %s Phonebook menu: %s %n%s %d to add contact "
          + "%n%s %d to update contact %n%s %d to delete contact %n%s %d to select all contacts %n%s %d to quit",
      STARS, STARS, PRESS, 1, PRESS, 2, PRESS, 3, PRESS, 4, PRESS, 5);
}
