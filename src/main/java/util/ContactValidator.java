package util;

import exceptions.InvalidInputException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactValidator {

  private static final String NAME_REGEX = "^[A-Z][a-z]{3,}";
  private static final String PHONE_REGEX = "^(\\+\\d{12})";

  public static void validate(final String firstName, final String lastName,
      final String phoneNumber) throws InvalidInputException {
    validateFirstName(firstName);
    validateLastName(lastName);
    validatePhone(phoneNumber);

  }

  private static void validateFirstName(final String firstName) throws InvalidInputException {
    final Pattern patternFirstName = Pattern.compile(NAME_REGEX);
    final Matcher matcher = patternFirstName.matcher(firstName);

    if (!matcher.matches()) {
      throw new InvalidInputException(firstName);
    }
  }

  private static void validateLastName(final String lastName) throws InvalidInputException {
    final Pattern patternLastName = Pattern.compile(NAME_REGEX);
    final Matcher matcher = patternLastName.matcher(lastName);

    if (!matcher.matches()) {
      throw new InvalidInputException(lastName);
    }
  }

  private static void validatePhone(final String phoneNumber) throws InvalidInputException {
    final Pattern patternPhone = Pattern.compile(PHONE_REGEX);
    final Matcher matcher = patternPhone.matcher(phoneNumber);

    if (!matcher.matches()) {
      throw new InvalidInputException(phoneNumber);
    }
  }
}
