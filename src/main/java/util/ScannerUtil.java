package util;

import static util.Constants.SCANNER;
import exceptions.ExceededCharacterLimitException;

public class ScannerUtil {

  public static final String getInputIfNotExceeded() throws ExceededCharacterLimitException {
    final String input = SCANNER.nextLine();

    if (input.length() <= 255) {
      return input;
    }
    throw new ExceededCharacterLimitException();
  }
}
