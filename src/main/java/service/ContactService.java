package service;

import exceptions.FileNotFoundException;
import exceptions.InvalidInputException;
import java.util.List;
import model.Contact;

public interface ContactService {

  void create(final Contact contact) throws FileNotFoundException, InvalidInputException;

  void delete(final Integer id) throws FileNotFoundException;

  void update(final Contact contact) throws FileNotFoundException;

  List<Contact> getAllContacts() throws FileNotFoundException;
}
