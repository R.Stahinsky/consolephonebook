package service.impl;

import static util.ContactValidator.validate;

import exceptions.FileNotFoundException;
import exceptions.InvalidInputException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import model.Contact;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.ContactService;

public class ContactServiceImpl implements ContactService {

  private static final Logger logger = LoggerFactory.getLogger(ContactServiceImpl.class);

  private static final String FILE_NAME_PATH = "src/main/resources/phonebook.csv";
  private static final File FILE = new File(FILE_NAME_PATH);
  private static final File TEMP = new File("src/main/resources/temp.csv");

  @Override
  public void create(final Contact contact)
      throws FileNotFoundException, InvalidInputException {

    validate(contact.getFirstName(), contact.getLastName(),
        contact.getPhoneNumber());
    try {
      final BufferedWriter writer = new BufferedWriter(new FileWriter(FILE, true));

      writeIntoFile(contact, writer);

      logger.info("Contact with id {} was created", contact.getId());
      writer.close();
    } catch (IOException e) {
      throw new FileNotFoundException(FILE.getPath());
    }
  }

  @Override
  public void delete(final Integer id) throws FileNotFoundException {
    try {
      final BufferedReader reader = new BufferedReader(new FileReader(FILE));
      final BufferedWriter writer = new BufferedWriter(new FileWriter(TEMP, true));

      removeFromFile(id, reader, writer);

      logger.info("Contact with id: {} was deleted", id);
      reader.close();
      writer.close();
    } catch (IOException e) {
      throw new FileNotFoundException(TEMP.getPath());
    }
    FILE.delete();
    TEMP.renameTo(FILE);
  }

  @Override
  public void update(final Contact contact) throws FileNotFoundException {
    try {
      final BufferedReader reader = new BufferedReader(new FileReader(FILE));
      final BufferedWriter writer = new BufferedWriter(new FileWriter(TEMP, true));

      removeFromFile(contact.getId(), reader, writer);
      writeIntoFile(contact, writer);

      logger.info("Contact with id: {} was updated", contact.getId());
      reader.close();
      writer.close();
    } catch (IOException e) {
      throw new FileNotFoundException(TEMP.getPath());
    }

    FILE.delete();
    TEMP.renameTo(FILE);
  }

  @Override
  public final List<Contact> getAllContacts() throws FileNotFoundException {
    try {
      final BufferedReader reader = new BufferedReader(new FileReader(FILE));

      final List<Contact> foundContacts = new ArrayList<>();
      String line;

      while ((line = reader.readLine()) != null) {
        String[] lines = (line.split(","));

        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        final Integer id = Integer.parseInt(lines[0]);
        final String firstName = lines[1];
        final String lastName = lines[2];
        final String phoneNumber = lines[3];
        final String additionalInfo = lines[4];
        final Date createDate = formatter.parse(lines[5]);

        final Contact contact = new Contact(id, firstName, lastName, phoneNumber, createDate,
            additionalInfo);

        foundContacts.add(contact);
      }
      logger.info("All contacts were found");
      return foundContacts;
    } catch (IOException | ParseException e) {
      throw new FileNotFoundException(FILE.getPath());
    }
  }

  private static void removeFromFile(final Integer id, final BufferedReader reader,
      final BufferedWriter writer)
      throws IOException {
    String line;

    while ((line = reader.readLine()) != null) {
      if (!line.contains(id.toString())) {
        writer.write(String.format("%s %n", line));
      }
    }
    logger.info("Contact with id: {} was successfully removed from file", id);
  }

  private static void writeIntoFile(final Contact contact, final BufferedWriter writer)
      throws IOException {

    final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);

    csvPrinter.printRecord(contact.getId(), contact.getFirstName(), contact.getLastName(),
        contact.getPhoneNumber(), contact.getAdditionalInfo(), LocalDateTime.now());

    csvPrinter.flush();
    csvPrinter.close();
    logger.info("Contact with id: {} was successfully written in file", contact.getId());
  }
}
