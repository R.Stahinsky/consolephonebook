package model;

import java.util.Date;
import java.util.Objects;

public class Contact {

  private Integer id;
  private String firstName;
  private String lastName;
  private String phoneNumber;
  private Date creatingDate;
  private String additionalInfo;

  public Contact() {
  }

  public Contact(Integer id, String firstName, String lastName, String phoneNumber,
      Date creatingDate, String additionalInfo) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.creatingDate = creatingDate;
    this.additionalInfo = additionalInfo;
  }

  public Contact(Integer id, String firstName, String lastName, String phoneNumber,
      String additionalInfo) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.additionalInfo = additionalInfo;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public Date getCreatingDate() {
    return creatingDate;
  }

  public void setCreatingDate(Date creatingDate) {
    this.creatingDate = creatingDate;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Contact contact = (Contact) o;
    return id.equals(contact.id) && Objects.equals(firstName, contact.firstName)
        && Objects.equals(lastName, contact.lastName) && Objects
        .equals(phoneNumber, contact.phoneNumber) && Objects
        .equals(creatingDate, contact.creatingDate) && Objects
        .equals(additionalInfo, contact.additionalInfo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, firstName, lastName, phoneNumber, creatingDate, additionalInfo);
  }

  @Override
  public String toString() {
    return "Contact{" +
        "id=" + id +
        ", firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", phoneNumber='" + phoneNumber + '\'' +
        ", creatingDate=" + creatingDate +
        ", additionalInfo='" + additionalInfo + '\'' +
        '}';
  }
}
