package view;

import static util.Constants.CONTACTS;
import static util.Constants.CREATE;
import static util.Constants.CREATE_CONTACT;
import static util.Constants.ENTER_CONTACT_ID;
import static util.Constants.SCANNER_INTEGER;
import static util.ContactUtil.generateId;
import static util.ScannerUtil.getInputIfNotExceeded;
import static view.MenuView.viewMenu;
import controller.ContactController;
import exceptions.ExceededCharacterLimitException;
import exceptions.FileNotFoundException;
import exceptions.InvalidInputException;
import java.util.List;
import model.Contact;

public class ContactView {

  private static final ContactController contactController = new ContactController();

  public static void addContact()
      throws FileNotFoundException, ExceededCharacterLimitException, InvalidInputException {
    System.out.println(CREATE_CONTACT);

    System.out.printf("%s first name: %n", CREATE);
    final String firstName = getInputIfNotExceeded();

    System.out.printf("%s last name: %n", CREATE);
    final String lastName = getInputIfNotExceeded();

    System.out.printf("%s phone number: %n", CREATE);
    final String phoneNumber = getInputIfNotExceeded();

    System.out.printf("%s additional info: %n", CREATE);
    final String additionalInfo = getInputIfNotExceeded();

    final Contact newContact = new Contact(generateId(), firstName, lastName, phoneNumber,
        additionalInfo);

    contactController.create(newContact);

    viewMenu();
  }

  public static void deleteContact() throws FileNotFoundException, ExceededCharacterLimitException {
    System.out.println(ENTER_CONTACT_ID);

    contactController.delete(SCANNER_INTEGER.nextInt());

    viewMenu();
  }

  public static void updateContact() throws ExceededCharacterLimitException, FileNotFoundException {
    System.out.println("Update contact");

    System.out.printf("First name: %n");
    final String firstName = getInputIfNotExceeded();

    System.out.printf("Last name: %n");
    final String lastName = getInputIfNotExceeded();

    System.out.printf("Phone number: %n");
    final String phoneNumber = getInputIfNotExceeded();

    System.out.printf("Additional info: %n");
    final String additionalInfo = getInputIfNotExceeded();

    System.out.printf("%s to update %n", ENTER_CONTACT_ID);
    final Integer id = SCANNER_INTEGER.nextInt();

    final Contact contactToUpdate = new Contact(id, firstName, lastName, phoneNumber,
        additionalInfo);

    contactController.update(contactToUpdate);

    viewMenu();
  }

  public static void  getAllContacts() throws FileNotFoundException, ExceededCharacterLimitException {
    System.out.println(CONTACTS);

    contactController.getAllContacts().forEach(System.out::println);

    viewMenu();
  }
}
