package view;

import static util.Constants.GOODBYE;
import static util.Constants.SCANNER;
import static util.Constants.SCANNER_INTEGER;
import static util.Constants.WELCOME;
import static view.ContactView.addContact;
import static view.ContactView.deleteContact;
import static view.ContactView.getAllContacts;
import static view.ContactView.updateContact;
import exceptions.ExceededCharacterLimitException;
import exceptions.FileNotFoundException;
import exceptions.InvalidInputException;

public class MenuView {

  public static void viewMenu()
      throws FileNotFoundException, ExceededCharacterLimitException, InvalidInputException {
    System.out.println(WELCOME);

    final String choice = SCANNER.nextLine();

    switch (choice) {
      case "1":
        addContact();
        break;
      case "2":
        updateContact();
        break;
      case "3":
        deleteContact();
        break;
      case "4":
        getAllContacts();
        break;
      case "5":
        System.out.println(GOODBYE);
        return;
    }
    SCANNER_INTEGER.close();
  }
}
